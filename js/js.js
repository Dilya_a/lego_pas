var popup 		= $("[data-popup]"),
	formstyler 	= $(".styler"),
	tabs 		= $(".tabs"),
	accordion   = $(".accordion"),
	dropzone  	= $('.dropzone'),
	scrollpane  = $('.scroll-pane');

if(popup.length){
  include("js/jquery.arcticmodal.js");
}

if (formstyler.length){
	include("js/jquery.formstyler.js");
}

if (dropzone.length) {
	include("js/dropzone.js");
}

if (tabs.length){
	include("js/jewelryTabs.js");
}

if (scrollpane.length){
	include("js/jquery.jscrollpane.min.js");
	include("js/jquery.mousewheel.js");
	include("js/mwheelIntent.js");
}


function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}



$(document).ready(function () {

	$(".menu_btn, .menu_close").on('click ontouchstart', function(e){
  		var body = $('body');
  		
  		body.toggleClass('nav_overlay');
    })

	$(".file__attach").change(function(){
		var filename = $(this).val().replace(/.*\\/, "");
		$(".checks__file-text").text(filename);
	});
	 

	// ================= Drop zone=================
	if (dropzone.length){
		dropzone.dropzone();
	}
	
	// =================modal start=================
		if(popup.length){
			popup.on('click',function(){
			    var modal = $(this).data("popup");
			    $(modal).arcticmodal();
			});
		}
    // =================modal end=================


    // =================Custom form elements=================

    if (formstyler.length){
    	$(formstyler).styler();
    }

    if(tabs.length){
    	tabs.init();
    }

    // =================modal start=================
		if(scrollpane.length){
			scrollpane.jScrollPane();
		}
    // =================modal end=================

    /* ------------------------------------------------
	ACCORDION START
	------------------------------------------------ */

		if($('.accordion_item_link').length){
			$('.accordion_item_link').on('click', function(){
				$(this)
				.toggleClass('active')
				.next('.sub-menu')
				.slideToggle()
				.parents(".accordion_item")
				.siblings(".accordion_item")
				.find(".accordion_item_link")
				.removeClass("active")
				.next(".sub-menu")
				.slideUp();
			});  
		}
		$('.accordion_item_link').on('click', function(e) {
		    e.preventDefault();
		});


	/* ------------------------------------------------
	ACCORDION END
	------------------------------------------------ */


});


$(window).load(function () {
	$('body').addClass('loaded');
})